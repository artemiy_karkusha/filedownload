<?php
ob_start();
session_start();
require "includes/connection.php";
include("includes/header.php");
if (isset($_SESSION["login"])) {
    $errors = array();
    $user = R::findOne("user", "login = ?", array($_SESSION["login"]));
    if ($_SESSION["login"] != $user->login) {
        $errors[] = "Користувача не знайдено";
    }
    if ($_SESSION["password"] != $user->password) {
        $errors[] = "Пароль не співпадає";
    }
    if (!empty($errors)) {
        header("Location:index.php");
    } else{
?>
    <div class="container mlogin">
        <div id="login">
            <h1>ВХІД</h1>
            <form name="loginform">
                <center>
                    <p>
                        Для скачивания EsetesI.zip нажмите <a href="download.php?download=ESETESI.zip">сюда</a><br>
                    </p>
                    <hr>
                    <p>
                        Для скачивания EsetesII.zip нажмите <a href="download.php?download=ESETESII.zip">сюда</a><br>
                    </p>
                    <hr>
                    <p>
                        Для скачивания EsetesIII.zip нажмите <a href="download.php?download=ESETESIII.zip">сюда</a><br>
                    </p>
                    <hr>
                    <p>
                        Для скачивания EsetesIV.zip нажмите <a href="download.php?download=ESETESIV.zip">сюда</a><br>
                    </p>
                    <hr>
                    <p>
                        Для скачивания EsetesV.zip нажмите <a href="download.php?download=ESETESV.zip">сюда</a><br>
                    </p>

                </center>
                <p style="float: right;"><a href="/index.php">На головну</a></p>
            </form>
        </div>
    </div>
<?php
    }
} else{
    header("Location:index.php");
}
include 'includes/footer.php';
ob_flush();
?>