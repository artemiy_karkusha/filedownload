<?php 
	session_start();
	require "includes/connection.php";
	if (isset($_SESSION["login"])) {
        $errors = array();
        $user = R::findOne("user", "login = ?", array($_SESSION["login"]));
        if ($_SESSION["login"] != $user->login) {
            $errors[] = "Користувача не знайдено";
        }
        if ($_SESSION["password"] != $user->password) {
            $errors[] = "Пароль не співпадає";
        }
        if (!empty($errors)) {
            header("Location:login.php");
        } else{
?>

            <?php include("includes/header.php"); ?>
            <div id="welcome">
			<h1>Привіт,<?php echo " " . $_SESSION["login"];?>!</h1>
			<p><a href="file.php">Скачати файли</a></p>
			<p><a href="logout.php">Вихід</a></p>
            </div>
<?php
        }
    } else{
    	header("Location:login.php");
    }
?>