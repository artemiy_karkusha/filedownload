<?php 
	session_start();
	ob_start();
    date_default_timezone_set('Europe/Kiev');
	require "includes/connection.php";
	include("includes/header.php");
	$data = $_POST;
	if (isset($data["signup"])) {
		$errors = array();
		if ($data["login"] == "") {
			$errors[] = "Введіть логін";
		}
		if (R::count("user", "login = ?", array($data["username"]))) {
			$errors[] = "Користувач з таким логіном вже існує";
		}
		if ($data["password"] == "") {
			$errors[] = "Введіть пароль";
		}
		if ($data["password"] != $data["password_2"]) {
			$errors[] = "Паролі не співпадають";
		}
		if ($data["capcha"] == "") {
			$errors[] = "Введіть капчу ";
		}
		if (md5($data["capcha"]) != $_SESSION['randomnr2']) {
			$errors[] = "Введіть капчу повторно";
		}
		if (empty($errors)) {
			$user = R::dispense("user");
			$user->login = $data["login"];
			$user->password = hash("sha256", $data["password"]);
			$user->law = "user";
			$user->reg_time = date(DATE_RSS);
			R::store($user);
			unset($data);
		} else {
            echo "<div class=\"error\">" . "ПОВІДОМЛЕННЯ: ". array_shift($errors) . "</div>";
		}
	}
?>
    <div class="container mregister">
    <div id="login">
	<h1>РЕЄСТРАЦІЯ</h1>
<form name="registerform" action="signup.php" method="post">

	<p>
		<label for="login">Логін<br>
		<input class="input" type="text" name="login" id="login" value="" size="20" autocomplete="off"></label>
	</p>
	
	<p>
		<label for="password">Пароль<br>
		<input class="input" type="password" name="password" id="password" value="" size="32" autocomplete="off"></label>
	</p>	
	<p>
		<label for="password_2">Введіть пароль повторно<br />
		<input class="input" type="password" name="password_2" id="password_2" value="" size="32" autocomplete="off"></label>
	</p>
	<p>
		<img src = "captcha.php" alt = "Каптча"><br>
		<label for="capcha">Введіть текст із зображенння</label>
  		<input class="input" type = "text" name = "capcha" value = "" id = "capcha" size = "10" autocomplete="off" onchange="capchaValid()">
	</p>
	<p>
		<input class="button" type="submit" name="signup" value="ЗАРЕГІСТРУВАТИСЯ" onclick="progresBar();">
	</p>
	<p>Ви вже зареєстровані? <br><a href="login.php" >Вхід тут</a>!</p>
</form>
    </div>
    </div>
<?php 
	ob_flush();
    include("includes/footer.php");
?>